SUBDIRS = endianness

all:
	@for p in $(SUBDIRS); do \
		PWD=$(PWD)/$$p make -C $$p all; \
	done

clean:
	rm -f *~
	for n in $(SUBDIRS); do $(MAKE) -C $$n clean; done